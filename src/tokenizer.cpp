#include "tokenizer.hpp"

#include <fstream>
#include <iostream>

Tokenizer::Tokenizer() : _tokenInfo() {
    AddToken(R"(^(namespace|target|core-directory|packet|stype|uint8|int8|uint16|int16|uint32|int32|string|float|raw|type|sequence|dynamic)[\(\)\]\s;])", TOKEN_KEYWORD, -1);
    AddToken(R"(^((0[xX][0-9a-fA-F]+)|([0-9]+))[\(\)\]\s;])", TOKEN_NUMBER, -1);
    AddToken("^([a-zA-Z0-9_]+)", TOKEN_IDENTIFIER);
    AddToken("^\"(.*?[^\\\\])\"", TOKEN_STRING);
    AddToken("^\\(", TOKEN_OPENING_BRACKET);
    AddToken("^\\)", TOKEN_CLOSING_BRACKET);
    AddToken("^\\{", TOKEN_OPENING_CURLY_BRACKET);
    AddToken("^\\}", TOKEN_CLOSING_CURLY_BRACKET);
    AddToken("^\\[", TOKEN_OPENING_SQUARE_BRACKET);
    AddToken("^\\]", TOKEN_CLOSING_SQUARE_BRACKET);
    AddToken("^<->", TOKEN_DIRECTION_IN_OUT);
    AddToken("^->", TOKEN_DIRECTION_IN);
    AddToken("^<-", TOKEN_DIRECTION_OUT);
    AddToken("^\\;", TOKEN_END_COMMAND);
    AddToken("^//(.*)", TOKEN_COMMENT);
}

void Tokenizer::AddToken(const std::string& regex, TokenType type, int lengthAdjust) {
    try {
        TokenInfo info;
        info.regex = regex;
        info.type = type;
        info.lengthAdjust = lengthAdjust;
        _tokenInfo.push_back(info);
    } catch(std::regex_error& err) {
        std::cout << "Failed to compile " << regex << " because: " << err.what() << std::endl;
    }
}

static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

std::vector<Token> Tokenizer::Tokenize(const std::string& file) {
    std::vector<Token> ret;

    // Open file
    std::ifstream fileStream(file);
    if(!fileStream.is_open()) return ret; // todo throw error

    auto lineNo = 0;

    for(std::string line; getline(fileStream, line);) {
        lineNo++;
        auto pos = 1;

        while(line.length() > 0) {
            ltrim(line);
            if(line.length() <= 0) break;

            std::cout << line << std::endl;

            bool doesMatch = false;

            // Check every token
            for(auto& tokenInfo: _tokenInfo) {
                std::smatch match;

                if(std::regex_search(line, match, tokenInfo.regex)) {
                    std::cout << "Match: " << match[0] << "(" << match.size() << ")" << std::endl;

                    Token token;
                    token.type = tokenInfo.type;
                    token.lineNo = lineNo;
                    token.pos = pos;

                    if(match.size() > 1) {
                        //std::cout << "Value: " << match[1] << std::endl;
                        token.value = match[1];
                    }

                    ret.push_back(token);
                    doesMatch = true;
                    auto length = match.length() + tokenInfo.lengthAdjust;
                    line = line.substr(length);
                    pos += length;
                    break;
                }
            }

            if(!doesMatch) {
                std::cout << "No match!" << std::endl;
                return ret; // todo throw exception
            }
        }
    }

    return ret;
}

std::string TokenToString(TokenType type)  {
    switch(type) {
        case TOKEN_KEYWORD:
            return "TOKEN_KEYWORD";
        case TOKEN_IDENTIFIER:
            return "TOKEN_IDENTIFIER";
        case TOKEN_STRING:
            return "TOKEN_STRING";
        case TOKEN_NUMBER:
            return "TOKEN_NUMBER";
        case TOKEN_OPENING_BRACKET:
            return "TOKEN_OPENING_BRACKET";
        case TOKEN_CLOSING_BRACKET:
            return "TOKEN_CLOSING_BRACKET";
        case TOKEN_OPENING_CURLY_BRACKET:
            return "TOKEN_OPENING_CURLY_BRACKET";
        case TOKEN_CLOSING_CURLY_BRACKET:
            return "TOKEN_KEYWORD";
        case TOKEN_OPENING_SQUARE_BRACKET:
            return "TOKEN_OPENING_SQUARE_BRACKET";
        case TOKEN_CLOSING_SQUARE_BRACKET:
            return "TOKEN_CLOSING_SQUARE_BRACKET";
        case TOKEN_DIRECTION_IN:
            return "TOKEN_DIRECTION_IN";
        case TOKEN_DIRECTION_OUT:
            return "TOKEN_DIRECTION_OUT";
        case TOKEN_END_COMMAND:
            return "TOKEN_END_COMMAND";
        case TOKEN_COMMENT:
            return "TOKEN_COMMENT";
        case TOKEN_EOF:
            return "TOKEN_EOF";
        case TOKEN_DIRECTION_IN_OUT:
            return "TOKEN_DIRECTION_IN_OUT";
    }
}